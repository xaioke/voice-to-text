<?php


namespace saber\VoiceToText\core;


use Pimple\Container;
use saber\VoiceToText\core\providers\ConfigProvider;
use saber\VoiceToText\core\providers\GuzzleHandlerProviders;
use saber\VoiceToText\core\providers\HttpClientServiceProvider;

/**
 * @property  $config;
 * Class ServiceContainer
 * @package saber\VoiceToText\core
 */
class ServiceContainer extends Container
{

    /**
     * 配置文件
     * @var array
     */
    protected $config = [];

    /**
     * 注册树
     * @var
     */
    protected $providers = [];


    /**
     * ServiceContainer constructor.
     * @param array $config
     * @param array $values
     */
    public function __construct(array $config = [], array $values = [])
    {
        $this->config = $config;
        parent::__construct($values);
        $this->registerProviders($this->getProviders());
    }

    /**
     * 获取配置文件
     * @return array
     */
    public function getConfig()
    {
        $base = [
            // http://docs.guzzlephp.org/en/stable/request-options.html
            'http' => [
                'timeout' => 30.0,
            ],
        ];

        return array_replace_recursive($base, $this->config);
    }


    /**
     * Return all providers.
     *
     * @return array
     */
    public function getProviders()
    {
        return array_merge([
            ConfigProvider::class,
            HttpClientServiceProvider::class,
            GuzzleHandlerProviders::class
        ], $this->providers);
    }


    /**
     * Magic get access.
     *
     * @param string $id
     *
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }


    public function shouldDelegate($id)
    {
        $this->config->get('delegation.enabled');
    }

    /**
     * Magic set access.
     *
     * @param string $id
     * @param mixed $value
     */
    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }

    /**
     * @param array $providers
     */
    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }

}