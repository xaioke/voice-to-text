<?php


namespace saber\VoiceToText\core;


use Psr\Http\Message\ResponseInterface;
use saber\VoiceToText\core\traits\HasHttpRequests;
use saber\VoiceToText\core\traits\RequestTrait;

class HttpCent
{
    use HasHttpRequests;



    /**
     *
     * @var ServiceContainer $app
     */
    protected $app;


    /**
     * 应用的id
     * @var
     */
    protected $app_id;

    /**
     * 前置url
     * @var array
     */
    protected $baseUri = '';


    protected $options = [];


    /**
     * http 客户端
     * @var \GuzzleHttp\Client $Cent
     */
    protected $httpClient;


    public function __construct(ServiceContainer $app)
    {

        $this->baseUri = $app->config['http']['base_url'];
        $this->app = $app;
    }


    /**
     *
     * GET 请求
     * @param string $url
     * @param array $query
     *
     * @return array|bool|float|int|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function httpGet(string $url, array $query = []):array
    {
        return $this->request($url, 'GET', ['query' => $query]);
    }


    /**
     * 请求处理
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return array|bool|float|int|object|string|null
     */
    protected function afterRequestHandle($response):array
    {
        return \GuzzleHttp\json_decode($response->getBody()->getContents(),true);
    }

    /**
     *
     * post request.
     * @param string $url
     * @param array $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function httpPost(string $url, array $data = [], array $query = []):array
    {
        return $this->request($url, 'POST', ['form_params' => $data, 'query' => $query]);
    }

    /**
     * JSON request.
     *
     * @param string $url
     * @param array $data
     * @param array $query
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function httpPostJson(string $url, array $data = [], array $query = []):array
    {
        return $this->request($url, 'POST', ['query' => $query, 'json' => $data]);
    }



}