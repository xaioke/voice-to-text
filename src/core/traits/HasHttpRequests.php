<?php


namespace saber\VoiceToText\core\traits;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use Psr\Http\Message\ResponseInterface;
use think\helper\Str;

trait HasHttpRequests
{
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * @var array
     */
    protected $middlewares = [];

    /**
     * @var \GuzzleHttp\HandlerStack
     */
    protected $handlerStack;

    /**
     * @var array
     */
    protected static $defaults = [
        'curl' => [
            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
        ],
    ];

    /**
     * Set guzzle default settings.
     *
     * @param array $defaults
     */
    public static function setDefaultOptions($defaults = [])
    {
        self::$defaults = $defaults;
    }

    /**
     * Return current guzzle default settings.
     *
     * @return array
     */
    public static function getDefaultOptions(): array
    {
        return self::$defaults;
    }

    /**
     * Set GuzzleHttp\Client.
     *
     * @param \GuzzleHttp\ClientInterface $httpClient
     *
     * @return $this
     */
    public function setHttpClient(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    /**
     * Return GuzzleHttp\ClientInterface instance.
     *
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface
    {
        if (!($this->httpClient instanceof ClientInterface)) {
            if (property_exists($this, 'app') && $this->app['http_client']) {
                $this->httpClient = $this->app['http_client'];
            } else {
                $this->httpClient = new Client(['handler' => HandlerStack::create($this->getGuzzleHandler())]);
            }
        }

        return $this->httpClient;
    }

    /**
     * Add a middleware.
     *
     * @param callable $middleware
     * @param string $name
     *
     * @return $this
     */
    public function pushMiddleware(callable $middleware, string $name = null)
    {
        if (!is_null($name)) {
            $this->middlewares[$name] = $middleware;
        } else {
            array_push($this->middlewares, $middleware);
        }

        return $this;
    }

    /**
     * Return all middlewares.
     *
     * @return array
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    /**
     * Make a request.
     *
     * @param string $url
     * @param string $method
     * @param array $options
     *
     * @return \Psr\Http\Message\ResponseInterface
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($url, $method = 'GET', $options = []): array
    {
        $method = strtoupper($method);

        $options = array_merge(self::$defaults, $options, ['handler' => $this->getHandlerStack()]);
        $options = $this->getAuthOption($method, $options);

        $options = $this->fixJsonIssue($options);
        if (property_exists($this, 'baseUri') && !is_null($this->baseUri)) {
            $options['base_uri'] = $this->baseUri;
        }
        $response = $this->getHttpClient()->request($method, $url, $options);

        $response->getBody()->rewind();
        return $this->responseHander($response);
    }


    public function  responseHander(ResponseInterface $response):array
    {
      return  \GuzzleHttp\json_decode($response->getBody()->getContents(),true);
    }
    /**
     *  获取更新参数
     * @param string $method
     * @param array $options
     * @return array
     */
    protected function getAuthOption(string $method, array $options): array
    {

        //todo::检查是否存在签名
        if (empty($this->app['config']['sign'])){
            //todo:: 抛出错误
        }

        $sign_data= [];
        switch ($method) {
            case "GET":
                $sign_data = $options['query'];
                break;
            case "POST";
                if (isset($options['json'])) {
                    $sign_data = $options['json'];
                } else {
                    $sign_data = $options['form_params'];
                }
                break;
        }
        $sign_data['timestamp'] = time();

        if (isset($this->app['config']['app_id'])){
            $sign_data['app_id'] = $this->app['config']['app_id'];
        }
        foreach ($sign_data as $k => $item) {
            if (is_array($item)){
                $item = json_encode($item,true);
            }
            $sign_data[$k]= (string)$item;
        }
        $sign_data['random'] = Str::random(5);
        foreach ($sign_data as $k => $item) {
            $sign_data[$k]= (string)$item;
        }
        $sign_data['sign']  = $this->sign($sign_data,$this->app['config']['sign']);
        switch ($method) {
            case "GET":
                $options['query'] = $sign_data;
                break;
            case "POST";
                if (isset($options['json'])) {
                    $options['json'] = $sign_data;
                } else {
                    $options['form_params'] = $sign_data;
                }
                break;
        }


        return  $options;
    }

    /**
     * 前面
     * @param $data
     * @param $key
     * @return string
     */
    protected function sign($data, $key)
    {
        ksort($data);
        return hash('sha256', json_encode($data) . $key);
    }

    /**
     * @param \GuzzleHttp\HandlerStack $handlerStack
     *
     * @return $this
     */
    public function setHandlerStack(HandlerStack $handlerStack)
    {
        $this->handlerStack = $handlerStack;

        return $this;
    }

    /**
     * Build a handler stack.
     *
     * @return \GuzzleHttp\HandlerStack
     */
    public function getHandlerStack(): HandlerStack
    {
        if ($this->handlerStack) {
            return $this->handlerStack;
        }

        $this->handlerStack = HandlerStack::create($this->getGuzzleHandler());

        foreach ($this->middlewares as $name => $middleware) {
            $this->handlerStack->push($middleware, $name);
        }

        return $this->handlerStack;
    }

    /**
     * @param array $options
     *
     * @return array
     */
    protected function fixJsonIssue(array $options): array
    {
        if (isset($options['json']) && is_array($options['json'])) {
            $options['headers'] = array_merge($options['headers'] ?? [], ['Content-Type' => 'application/json']);

            if (empty($options['json'])) {
                $options['body'] = \GuzzleHttp\json_encode($options['json'], JSON_FORCE_OBJECT);
            } else {
                $options['body'] = \GuzzleHttp\json_encode($options['json'], JSON_UNESCAPED_UNICODE);
            }

            unset($options['json']);
        }

        return $options;
    }


    /**
     * Get guzzle handler.
     *
     * @return callable
     */
    protected function getGuzzleHandler()
    {

        if (property_exists($this, 'app') && isset($this->app['guzzle_handler'])) {


            return is_string($handler = $this->app['guzzle_handler'])
                ? new $handler()
                : $handler;
        }

        return \GuzzleHttp\choose_handler();
    }

}