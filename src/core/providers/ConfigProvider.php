<?php

namespace saber\VoiceToText\core\providers;

use Pimple\ServiceProviderInterface;
use saber\VoiceToText\core\Config;

class ConfigProvider implements ServiceProviderInterface
{

    public function register(\Pimple\Container $pimple)
    {
        if (!isset($pimple['config']) ){
            $pimple['config'] = function ($app) {
                return new Config($app->getConfig());
            };
        }
    }
}