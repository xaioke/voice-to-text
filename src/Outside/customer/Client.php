<?php


namespace saber\VoiceToText\Outside\customer;


use Psr\Http\Message\ResponseInterface;
use saber\VoiceToText\core\HttpCent;

class Client extends HttpCent
{
    /**
     * 获取当前用户信息
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get():array
    {
       return $this->httpGet('/customer/outside/get');
    }


    /**
     * 用量报警通知
     * @param string $notify_url 通知回调地址
     * @param int $surplu_time 用量小于多少
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function nits(string $notify_url, int $surplu_time  ):array
    {
        return $this->httpPost('/customer/outside/nits',['notify_url'=>$notify_url,'surplu_time'=>$surplu_time]);
    }

}