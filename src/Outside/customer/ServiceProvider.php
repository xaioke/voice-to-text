<?php


namespace saber\VoiceToText\Outside\customer;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        if (!isset($pimple['customer'])) {
            $pimple['customer'] = function ($app) {
                return new Client($app);
            };
        }
    }
}