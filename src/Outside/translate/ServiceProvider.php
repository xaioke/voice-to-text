<?php


namespace saber\VoiceToText\Outside\translate;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use saber\VoiceToText\core\Config;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        if (!isset($pimple['translate'])) {
            $pimple['translate'] = function ($app) {
                return new Client($app);
            };
        }
    }
}