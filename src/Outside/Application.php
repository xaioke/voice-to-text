<?php


namespace saber\VoiceToText\Outside;


use saber\VoiceToText\core\ServiceContainer;

/**
 * @property \saber\VoiceToText\Outside\translate\Client $translate 语音翻译
 * @property \saber\VoiceToText\Outside\customer\Client $customer 用户详情
 * Class Application
 * @package saber\VoiceToText\inside
 */
class Application extends ServiceContainer
{
    protected $providers=[
        \saber\VoiceToText\Outside\translate\ServiceProvider::class,
        \saber\VoiceToText\Outside\customer\ServiceProvider::class
    ];

    
}