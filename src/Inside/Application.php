<?php


namespace saber\VoiceToText\Inside;


use saber\VoiceToText\core\ServiceContainer;

/**
 * @property \saber\VoiceToText\Inside\translate\Client $translate 语音翻译
 * @property \saber\VoiceToText\Inside\customer\Client $customer 用户详情
 * Class Application
 * @package saber\VoiceToText\inside
 */
class Application extends ServiceContainer
{
    protected $providers=[
        \saber\VoiceToText\Inside\translate\ServiceProvider::class,
        \saber\VoiceToText\Inside\customer\ServiceProvider::class

    ];

    
}