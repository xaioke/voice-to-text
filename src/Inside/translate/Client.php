<?php


namespace saber\VoiceToText\Inside\translate;


use Psr\Http\Message\ResponseInterface;
use saber\VoiceToText\core\HttpCent;

class Client extends HttpCent
{
    /**
     * 语音翻译
     * @param string $notify_url 回调通知地址
     * @param string $file_url 文件下载地址
     * @param array  $attach   额外参数
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function translate(string $notify_url, string $file_url, $attach = []):array
    {
       return $this->httpPost('/translate/inside/', ['notify_url' => $notify_url, 'file_url' => $file_url, 'attach' => \GuzzleHttp\json_encode($attach)]);
    }


    /**
     * 通过 request_id 获取请求结果
     * @param $request_id
     * @return array|bool|float|int|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string  $request_id):array
    {
        return $this->httpGet('/translate/inside/get', ['request_id' => $request_id]);
    }

    /**
     * 获取请求列表
     * @param $limit
     * @param $page
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(int $limit, int $page):array
    {
       return $this->httpGet('/translate/inside/list',['limit'=>$limit,'page'=>$page]);
    }
}